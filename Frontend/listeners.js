function resize(event){
    event.preventDefault();

    renderer.setSize(window.innerWidth, window.innerHeight);
    camera.aspect = window.innerWidth / window.innerHeight;

    camera.updateProjectionMatrix();
}

function MouseClick() {
    var time = new Date();
    last_enter = time.getTime();
    if((last_enter-first_enter) < 300){
        double_click = true;
    }
    first_enter = last_enter;
}

window.addEventListener("resize", resize);

window.addEventListener("click", MouseClick);

