function createStats() {
    var stats = new Stats()
    stats.setMode(0)

    stats.domElement.style.position = 'absolute'
    stats.domElement.style.left = '0'
    stats.domElement.style.top = window.innerHeight+'px'
    stats.domElement.style.height = 'auto'
    stats.domElement.style.width = 'auto'

    return stats
}

function updateStats() {
    if(mode === in_post){
        stats.domElement.style.top = '0'
    } else{
        stats.domElement.style.top = window.innerHeight+'px'
    }
    stats.update()
}

function to_xy_coordenates (pos) {
    let object_pos = new THREE.Vector3(pos.x, pos.y, pos.z)
    var vector = new THREE.Vector3()
    camera.updateMatrixWorld()
    pos = pos.project(camera)
    vector.x = Math.round((pos.x + 1)/2 * window.innerWidth)
    vector.y = -Math.round((pos.y - 1)/2 * window.innerHeight)
    vector.z = camera.position.distanceTo(object_pos)
    return vector
}

function get_stars_positions(radius){
    var pos = new THREE.Vector3
    var magnitud_desvio = Math.random() * radius
    var desvio = new THREE.Vector3

    pos.x = Math.random() * 2 - 1
    pos.y = Math.random() * 2 - 1
    pos.z = Math.random() * 2 - 1

    desvio.x = Math.random() * 2 - 1
    desvio.y = Math.random() * 2 - 1
    desvio.z = Math.random() * 2 - 1

    pos.normalize()
    pos.multiplyScalar(radius)
    desvio.normalize()
    desvio.multiplyScalar(magnitud_desvio)

    pos.add(desvio)

    return pos

}

function valid_position(pos, index) {
    var res = true
    for(var k = 0; k <= index; k++){
        if(collapse(pos, star[k])){
            res = false
        }
    }
    return res
}

function get_background_positions(radius){
    var pos = new THREE.Vector3
    pos.x = Math.random() * 2 - 1
    pos.y = Math.random() * 2 - 1
    pos.z = Math.random() * 2 - 1

    pos.normalize()
    pos.multiplyScalar(radius)

    return pos
}

function generate_background() {
    for(var i = 0;i<bg_number;i++){
        var pos = get_background_positions(bg_radius)
        bg_positions[i*3] = pos.x
        bg_positions[i*3+1] = pos.y
        bg_positions[i*3+2] = pos.z
    }
    bg_geometry.setAttribute('position', new THREE.BufferAttribute(bg_positions, 3))
    var mesh = new THREE.Points(bg_geometry, bg_material)
    scene.add(mesh)
}

function generate_stars(cant, usersData){
    for(var i = 0;i<cant;i++){
        var star_geometry = new THREE.CircleGeometry(5, star_poly)
        var star_material = new THREE.MeshBasicMaterial({color: "#ffffff"})
        star[i] = new THREE.Mesh(star_geometry, star_material)
        do {
            var pos = get_stars_positions(galaxy_radius)
        }while(!valid_position(pos, i))

        star[i].position.set(pos.x, pos.y, pos.z)
        mapa.set(star[i],usersData[i])
        scene.add(star[i])
    }
}

function destroy_stars(cant) {
    for(var i = 0; i<cant; i++){
        scene.remove(star[cant-i-1])
        star.pop()
    }
}

function rotate_stars_2_camera(){
    for(var i = 0;i<stars_number;i++){
        star[i].lookAt(camera.position)
    }
}

function update_cursor() {
    var dir = new THREE.Vector3
    camera.getWorldDirection(dir)
    dir.multiplyScalar(40)
    dir.add(camera.position)

    cursor.position.x = dir.x
    cursor.position.y = dir.y
    cursor.position.z = dir.z
    cursor.lookAt(camera.position)
}

function distance_between(pos, star) {
    var x1 = pos.x
    var y1 = pos.y
    var z1 = pos.z
    var x2 = star.position.x
    var y2 = star.position.y
    var z2 = star.position.z

    var distance_square = (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2-z1)*(z2-z1)
    return Math.sqrt(distance_square)
}

function collapse(pos, star) {
    var res = false
    if(distance_between(pos, star) <= 4*5){
        res = true
    }
    return res
}

function girar_estrellas(stars) {
    for(var i = 0; i<stars_number; i++){
        var vector_director = new THREE.Vector3(star[i].position.x, star[i].position.y, star[i].position.z)
        var rot = new THREE.Vector3(0,1,0)

        if(!stars.includes(star[i])){
            vector_director.applyAxisAngle(rot, rotate_speed)
        }
        star[i].position.x = vector_director.x
        star[i].position.y = vector_director.y
        star[i].position.z = vector_director.z
    }
}

function generate_line(stars, id) {
    scene.remove(line)
    var puntos_linea = []
    let centre_pos
    stars.forEach(function (star) {
        if(mapa.get(star).postId === id){
            centre_pos = star.position
        }
    })

    stars.forEach(function (star) {
        puntos_linea.push(centre_pos)
        puntos_linea.push(star.position)
    })

    line_geometry = new THREE.BufferGeometry().setFromPoints(puntos_linea)
    line_material = new THREE.LineBasicMaterial({ color: "#444444"})
    line = new THREE.Line(line_geometry, line_material)
    scene.add(line)
}

function back_to_normal() {
    for(var i = 0; i < stars_number; i++){
        star[i].material.color.set("#ffffff")
        star[i].scale.set(1,1,1)
    }
    rotate_speed = on
    destroy_text()
}

function highlight_stars(postInfo, usersData) {
    let postRelations = getRelationsId(postInfo, usersData)
    let selected_stars = new Array(postRelations.length)
    let counter = 0
    mapa.forEach(function (info, star) {
        if(postRelations.includes(info.postId)){
            star.scale.set(4,4,4)
            selected_stars[counter] = star
            counter += 1
        }
        else{
            star.scale.set(0.5,0.5,0.5)
            star.material.color.set("#606060")
        }
    })

    return selected_stars
}

function star_in_center(objects) {
    let res = false
    objects.forEach(function (obj) {
        if(mapa.get(obj.object) !== undefined){
            res = true
        }
    })
    return res
}

function waiting_for_button() {
    back_to_normal()
    window.scrollTo(0,window.innerHeight)
    mode = hunting
}

function setup_iframe(url) {
    var frame = document.getElementById("frame")
    frame.setAttribute("src", url)
    var div = document.getElementById("frame_div")
    div.scrollTo(0,0)
}

function get_2D_positions(stars) {
    let posiciones = []
    stars.forEach(function (star) {
        let pos = new THREE.Vector3()
        pos.x = star.position.x
        pos.y = star.position.y
        pos.z = star.position.z
        posiciones.push(to_xy_coordenates(pos))
    })

    return posiciones
}

function map(value, min_in, max_in, min_out, max_out){
    return (value - min_in) * (max_out - min_out) / (max_in - min_in) + min_out
}

function offset_positions(positions) {
    let posiciones = []
    positions.forEach(function (current_pos) {
        let x_offset = 0
        if(current_pos.z < 1000) x_offset = map(current_pos.z, 50, 1000, 100, 10)
        current_pos.x = Math.round(current_pos.x + x_offset)
        posiciones.push(current_pos)
    })

    return posiciones
}

function destroy_text() {
    for(let i = 0; i<texts.length; i++){
        let id = "title"+i
        elem = document.getElementById(id)
        if(elem !== null){
            elem.parentNode.removeChild(elem)
        }
    }
}

function choose_size(distance) {
    if(distance<1500) return "xx-large"
    if(distance<2000) return "x-large"
    if(distance<2500) return "large"
    if(distance<3000) return "medium"
    return "small"
}

function write_text(positions, titles) {
    texts = []
    for(let i=0;i < positions.length; i++){
        let size = choose_size(positions[i].z)
        texts[i] = document.createElement("texto")
        let id = "title"+i
        texts[i].setAttribute('id', id)
        texts[i].style.position = 'absolute'

        texts[i].style.color = "white"
        texts[i].style.zIndex = 1    // if you still don't see the label, try uncommenting this
        texts[i].style.backgroundColor = "none"
        texts[i].innerHTML = titles[i].toString()
        texts[i].style.width = "auto"
        texts[i].style.height = "auto"
        texts[i].style.top = window.innerHeight + positions[i].y + 'px'
        texts[i].style.left = positions[i].x + 'px'
        texts[i].style.fontSize = size
        texts[i].style.fontFamily = "Helvetica"
        document.body.appendChild(texts[i])
    }
}

function update_text(stars) {
    destroy_text()
    let posiciones = get_2D_positions(stars)
    posiciones = offset_positions(posiciones)
    let titles = new Array(posiciones.length)
    for(let i = 0;i<titles.length;i++){
        titles[i] = fix_name(mapa.get(stars[i]).name)
    }
    write_text(posiciones, titles)
}

function fix_name(name) {
    while(name.indexOf("-") !== -1){ //Saco los guiones
        name = name.replace("-", " ")
    }

    name = name[0].toUpperCase() + name.slice(1) //Pongo la primer letra mayuscula

    return name
}

function get_post_info(objetos) {
    let res = {
        postId: -1,
        name: "",
        relations: [],
    }
    objetos.forEach(function (obj) {
        if(mapa.get(obj.object) !== undefined && res.postId === -1){
            res = mapa.get(obj.object)
        }
    })
    
    return res
}

function update_raycaster(usersData) {
    //Setup del raycaster
    camera.getWorldDirection(dir_camara)
    raycaster.set(camera.position,dir_camara, 50, 999999)
    var intersecados = raycaster.intersectObjects(scene.children, false) //Array con todos los objetos intersecados por el raycaster (ordenados por distancia creciente).

    switch(mode){
        case hunting:
            window.scrollTo(0,window.innerHeight)
            highlighted_stars = []
            back_to_normal()
            scene.remove(line)
            if(center_star_index >= 0 && center_star_index < stars_number){
                star[center_star_index].material.color.set("#ffffff")

            }
            center_star_index = -1
            if(star_in_center(intersecados)){
                mode = on_sight
            }
            if(double_click){
                double_click = false
            }
            break
        case on_sight:
            window.scrollTo(0,window.innerHeight)
            highlighted_stars = []

            if(star_in_center(intersecados)){
                let postInfo = get_post_info(intersecados)
                highlighted_stars = highlight_stars(postInfo, usersData)
                generate_line(highlighted_stars, postInfo.postId)
                update_text(highlighted_stars)

                if(double_click){
                    double_click = false
                    rotate_speed = off
                    mode = selected
                }
            }
            else{
                mode = hunting
            }
            break
        case selected:
            window.scrollTo(0,window.innerHeight)
            update_text(highlighted_stars)
            if(double_click){
                destroy_text()
                double_click = off
                if(star_in_center(intersecados)){
                    let post_info_name = get_post_info(intersecados).name
                    var post_url = "http://proyectoidis.org/"
                    post_url += post_info_name
                    setup_iframe(post_url)
                    window.scrollTo(0,0)
                    unlockPointer()
                    mode = in_post
                }
                else{
                    mode = hunting
                }
            }
            break
        case in_post:
            highlighted_stars = []
            //waiting for button
            if(double_click){
                double_click = off
            }
            break
        default:
            highlighted_stars = []
            mode = hunting
            break
    }

    return highlighted_stars
}

function update_stars(usersData) {
    stars_number = get_stars_number(usersData)
    if(stars_number !== stars_number_buffer){
        destroy_stars(stars_number_buffer)
        stars_number_buffer = stars_number
        generate_stars(stars_number, usersData)
    }
}

function update_div_height() {
    var div = document.getElementById("frame_div")
    div.style.height = (window.innerHeight-button_height).toString(10)+'px'
}

let update = (usersData) => {
    update_stars(usersData)
    update_div_height()
    var highlighted_stars = (typeof update_raycaster(usersData) === 'undefined') ? [-1] : update_raycaster(usersData)
    rotate_stars_2_camera()
    girar_estrellas(highlighted_stars)

    //controls.update()
    update_cursor()
    updateStats()
}

function render() {
    renderer.render(scene, camera)
}

function resize(event){
    event.preventDefault()

    renderer.setSize(window.innerWidth, window.innerHeight)
    camera.aspect = window.innerWidth / window.innerHeight

    camera.updateProjectionMatrix()
}

let game_loop = (usersData) => {
    requestAnimationFrame(() => {
        game_loop(usersData)
    })
    update(usersData)
    if(render_time > render_threshold) {
        render()
        render_time = 0
    } else render_time += 1
}

function get_stars_number(usersData){
    return usersData.length
}
