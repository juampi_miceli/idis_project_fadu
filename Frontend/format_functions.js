let getRelationName = (relationUrl) => {
	const structure = '/[\-a-z0-9]+/'
	var relationName = relationUrl.match(structure)
	return relationName[0].substring(1, relationName[0].length-1)
}

let getRelationIdByName = (relationName, usersData) => {
	let userId = -1
	usersData.forEach(element => {
		if (element.name == relationName){userId = element.postId}
	})
	return userId
}

function getRelationsId(postInfo, usersData) {
	let relationsById = new Array()
	relationsById.push(postInfo.postId)
	postInfo.relations.forEach(element => {
		const relationName = getRelationName(element)
		const relationId = getRelationIdByName(relationName, usersData)
		relationsById.push(relationId)
	});
	return relationsById
}