## IDIS Project

The Idis Project consisted of 2200 different instagram style posts telling the history of image and sound design. We took that and integrated it into a knowledge constellation, the stars being each of these posts. We also added code to enable traversing that constellation with Leap Motion.

Demo video:

![demo-video](./demoVideo.mp4)