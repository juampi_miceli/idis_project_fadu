<?php

require __DIR__ . '/conn.php';

$getAllPostsQuery = "SELECT ID,post_name,post_content FROM `wp_posts` WHERE post_status='publish' AND post_type='post' ORDER BY ID ASC";

function getAllPostsData($conn,$sql){
	$query = mysqli_query($conn, $sql);
	$raw_data = getRawQueryData($query);

	$posts_data = array();
	for ($i=0; $i < sizeof($raw_data); $i++) {
		$posts_data[$i]['postId'] = $raw_data[$i]['ID'];
		$posts_data[$i]['name'] = $raw_data[$i]['post_name'];
		$posts_data[$i]['relations'] = getRelations($raw_data[$i]['post_content']);
	}
	return $posts_data;
}

function getRawQueryData($query){
	$raw_data = array();
	while ($queryResult = mysqli_fetch_assoc($query)){
		$raw_data[] = $queryResult;
	}
	return $raw_data;
}

function getRelations($text){
	$relationsStructure = '/<a href="(http|https):\/\/proyectoidis\.org\/[a-z\-0-9]+\/">/';
	preg_match_all($relationsStructure, $text, $relations);
	return $relations[0];
}

function getRelationName($relationUrl){
	$structure = '/[\-a-z0-9]+/';
	preg_match_all($structure, $relationUrl, $relationName);
	$last_index = sizeof($relationName[0]) - 1;
	return $relationName[0][$last_index];
}

function getPostIdByName($postName, $posts){
	$found = false;
	$i = 0;
	$postId = -1;
	while ($i < sizeof($posts) && !$found) {
		if ($posts[$i]['post_name'] == $postName) {
			$postId = $posts[$i]['ID'];
			$found = true;
		}
	}
	return $postId;
}

function getRelationsIdAndNameByUrl($relationsArray, $posts){
	$relationsById = array();
	foreach ($relationsArray as $relationUrl){
		$postName = getRelationName($relationUrl);
		$postId = getPostIdByName($postName, $posts); //postId = -1 if not found
		$relationsById[$postId] = $postName; // key is postId and value is postName
	}
	return $relationsById;
}

$posts = getAllPostsData($conn,$getAllPostsQuery);
echo json_encode($posts);